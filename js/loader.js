(function()
{
'use strict';

var Loader = {

	selectorPreload: document.body,  	
	preloader: "",						//DIV HOLDER WITH IMGS FOR LOAD
	overlay:"",
	loadBar: "",
	loadAmt: "",

	items: [],
	perc: 0,	
	
	doneStatus: 2, 		//Font + JSON 
	doneNow: 0,

	//...... 
	testCSS:  function( prop ) 
	{
	    return prop in document.documentElement.style;
	},
	//...... 
	init: function() 
	{
		//...... 
		this.selectorPreload.style.display = "block";	
		this.spawnLoader();							//LOAD ELEMENTS (LOAD INFO, LOAD BAR, LOAD OVERLAY)
		this.getImages( this.selectorPreload );		//IMGS TOTAL
		this.createPreloading();					//IMGS LOAD
	},
	//...... FUNC. AUX. CREATE DOM ELEMENTS
	fragBuild:  function (qualTag, qualContent)
	{						
		var elem_c = document.createElement(qualTag);
		
		if (qualContent)
		{
			elem_c.innerHTML  =  qualContent;			
		}
		
		return elem_c;
	},
	//...... LOAD BAR OVERLAY, LOAD BAR PROGRESS, LOAD BAR INFO; 
	spawnLoader: function() 
	{
		var preloadHolder = this.selectorPreload;			
		//......		
		var height  	=  window.innerHeight;				
		var width 		=  window.innerWidth;				
		var position 	=  "fixed";					
		var left 		=  document.body.offsetLeft;		
		var top  		=  document.body.offsetTop;			
		//...... DIV OVERLAY BODY ELEMENTS
		this.overlay  =  this.fragBuild ('div');		
		preloadHolder.appendChild( this.overlay );
		//...... 	
		this.loadAmt  =  this.fragBuild ('div', 'LOADING...');	
		this.overlay.appendChild( this.loadAmt );
		//...... FORMATAR DIV OVERLAY
		//TESTS	Loader.overlay.style.opacity   =  0.5;	
		this.overlay.style.position   =  position;
		this.overlay.style.top  		=  top;
		this.overlay.style.left  		=  left;
		this.overlay.style.width  	=   width + "px";
		this.overlay.style.height  	=  height + "px";
		this.overlay.style.backgroundColor =  "#FFFFFF";
		this.overlay.style.zIndex  	=  9999;							
		//...... LOAD BAR
		this.loadBar  =  this.fragBuild ('div');		
		this.overlay.appendChild( this.loadBar );
		//...... FORMAT DIV LOADBAR
		this.loadBar.style.position   =  "relative";
		this.loadBar.style.top  		=  "50%";
		this.loadBar.style.width  	=  "200px";
		this.loadBar.style.height  	=  "2px";
		this.loadBar.style.backgroundColor  =  "#333333";
		//...... FORMATAR DIV loadAmt
		this.loadAmt.style.position   =  "relative";
		this.loadAmt.style.top  		=  "50%";
		this.loadAmt.style.left  		=  "50%";
		this.loadAmt.style.color  	=  "#666";
		this.loadAmt.style.fontFamily =  "Arial,Helvetica,sans-serif";
		this.loadAmt.style.fontSize   =  "14px";
		this.loadAmt.style.fontWeight	=  "bold";
		this.loadAmt.style.lineHeight	=  "50px";
		this.loadAmt.style.height		=  "50px";
		this.loadAmt.style.width		=  "400px";				
		this.loadAmt.style.margin		=  "-60px 0 0 -180px";	
		this.loadAmt.style.textAlign	=  "center";						
	},
	//...... GET ALL IMAGES FOR LOAD
	getImages: function(selector) 
	{
			var url, urlAux, urlAux0, valor, elem_img  =  "";
			
			var elements  =  selector.querySelectorAll('*:not(script)');
					
			for ( var i = 0; i < elements.length; i++ ) 
			{
	 				//...... CSS BACKGROUND-IMAGE...
					valor  =  document.defaultView.getComputedStyle( elements[i], null ).getPropertyValue( "background-image" );
					if( valor  !=  "none" &&  valor.indexOf("url") != -1 )
					{
							//...... IS IT MULTIPLE?
							if( valor.indexOf(",")  !=  -1 )					
							{								
									urlAux  =  valor.split(",");
									 
									for( var n = 0; n < urlAux.length; n++)
									{
											urlAux0  =  this.retirarURL( urlAux[n] );				
											
											if (  this.imgAlreadyAssigned( urlAux0 ) == false )
											{												
	 												
													if( urlAux0.indexOf(".gif") != -1 ||  urlAux0.indexOf(".jpg") != -1  ||  urlAux0.indexOf(".png") != -1 )
													{
															this.items.push( urlAux0 );
													}	
											}	
									}
							}else{
	 								//......
									valor  =  this.retirarURL(valor);
									
									if ( this.imgAlreadyAssigned( valor ) === false )
									{												
											
											if( valor.indexOf(".gif") !== -1 ||  valor.indexOf(".jpg") !== -1  ||  valor.indexOf(".png") !== -1 )
											{
													
													this.items.push( valor );
											}	
									}	
							}		
					}
					//...... GET IMGs TAGS
					if( elements[i].nodeName === "IMG")
					{
							elem_img  =  elements[i].getAttribute('src');

							if( elem_img !== "undefined"  &&  elem_img !== "" )	
							{
									if (  this.imgAlreadyAssigned( elem_img ) == false )
									{												
											this.items.push( elem_img );	
									}
							}
					}	
			};
			//......
	},//getImages  END!	
	//...... FUNC. AUX.
	retirarURL: function(valor)						
	{
			var elem  =  valor;
					
		  	elem  =  elem.replace("url(\"", "");
		    elem  =  elem.replace("url(", "");		
			elem  =  elem.replace("\")", "");
			elem  =  elem.replace(")", "");
			elem  =  elem.replace(" ", "");			
					
			return elem;
	},
	//...... FUNC. AUX.
	imgAlreadyAssigned: function( qualImg )			
	{
			var imagemAVerificar  =  qualImg;
			var crtl_  =  false;
			var total  =  this.items.length;
			//......
			for( var k=0; k < total; k++ )
			{		
					//......
					if( this.items[k] == imagemAVerificar )
					{							
							crtl_ = true;		
							break;	
					}				
			}
			//......
			return crtl_;
	},
	//...... LOAD IMGS
	createPreloading: function() 
	{
		//...... HOLDER TEMP. IMGS FOR LOAD
		this.preloader  =  this.fragBuild('div');				
		this.selectorPreload.appendChild( this.preloader );	
		this.preloader.style.height  		=  "0px";
		this.preloader.style.width  		=  "0px";
		this.preloader.style.overflow  	=  "hidden";
		
		var length  		=  this.items.length;
		this.doneStatus  	+=  length;				
		console.log( "FUNC. createPreloading »»» TOTAL IMGS:::" +	length );		 	
			
		//......
		function onLoadImage(event, valor)
		{
			//console.log( "onLoadImage::: " + e.currentTarget.getAttribute('src')  );
			var main = valor;
			main.imgCallback();
			event.currentTarget.removeEventListener("load",  onLoadImage );	
			event.currentTarget.removeEventListener("error",  onLoadError );		
		}
		//......
		function onLoadError(e)
		{
			console.log( "onLoadError:::" + e.currentTarget.getAttribute('src')  );					
		}
		//......
		var imgLoad  =  "";
		var main = this;

		for ( var i = 0; i < length; i++ ) 
		{
			imgLoad  =  this.fragBuild('img');									
			
			imgLoad.setAttribute('src', this.items[i] );
								
			imgLoad.addEventListener("load",  function(){onLoadImage(event, main)} );
			imgLoad.addEventListener("error", onLoadError );
			
			this.preloader.appendChild( imgLoad );						
		}
		//...... 
	},
	//...... IMG LOAD SUCCESS
	imgCallback: function()
	{
			this.doneNow ++;
			//console.log( "LOADER.JS       imgCallback »»» Loader.doneNow ::: " + Loader.doneNow);
			this.animateLoader();
	},
	//...... LOAD BAR PROGRESS
	animateLoader: function() 
	{
		var loaderCalc  =  this.doneNow  /  this.doneStatus;		
		this.perc  =  Math.floor(( loaderCalc )*100);	
		this.loadBar.style.width  =  this.perc + "%";
		
		//...LOAD TERMINOU
		if ( this.perc  >=  100 )  
		{	
			this.endLoad();	
		
		//...A LOADAR					
		} else {		
				this.loadAmt.innerHTML  =  "LOADING...";  //+  Loader.perc  +  "%";
		}

	},//animateLoader: ...END!
	//......
	endLoad: function ()
	{
		console.log("Loader »»» func endLoad!!!!!!!");								
		this.loadAmt.innerHTML  =  "LOAD COMPLETE!";
		var main = this;
		
		setTimeout( this.desativarElementsLoader, 1000, main ); 
	},
	//......
	desativarElementsLoader: function (valor)
	{
		var main = valor;
		console.log("Loader »»» func desativarElementsLoader!!!!!!!");	
		//REMOVE TAG IMGS FOR LOAD
		main.selectorPreload.removeChild( main.preloader );
		//REMOVE LOADER OVERLAY
		main.selectorPreload.removeChild( main.overlay );	
	}
	//......
};//Loader  END!

window.Loader  =  Loader;
})(window);